FROM bas0korver/jmusicbot:latest

COPY ./docker-entrypoint.sh ./install_serve.sh ./

RUN sh install_serve.sh

RUN chmod +x docker-entrypoint.sh
